class Student {
  constructor(name1) {
    this.name = name1;
  }
  //Instance Method
  biodata() {
    return `Hello ${this.name}`;
  }
}

class Player extends Student {
  constructor(name1, game) {
    super(name1);
    this.game1 = game;
  }
  biodata() {
    return `Hello ${this.name} , I am a ${this.game1}`;
  }
}

let obj1 = new Player("Saumya", "Gamer");

console.log(obj1.biodata());
