function User(fname, score) {
  const name = fname.split(" ");
  this.fName = name[0];
  this.lName = name[1];
  this.score = score;
  this.getFullName = function () {
    return `Full name is ${this.fName} ${this.lName}`;
  };
}
User.prototype.getGrade = function () {
  if (this.score > 59) return "P&P";
  else return "Fail";
};
User.prototype.getScore = function () {
  console.log("Your Grade is : " + this.getGrade()); // Calling GetGrade
  return `Score is ${this.score}`;
};
const sumannn = new User("Suman Acharyya", 80);

if (sumannn.hasOwnProperty("getFullName")) {
  console.log(sumannn.hasOwnProperty("getFullName"));

  console.log(sumannn.getFullName());
  console.log(sumannn.getScore());
}
