// const gamer1 = {
//   name: "Cohh",
//   type: "RPG",
// };

// const gamer2 = {
//   name: "Viss",
//   type: "Apex",
// };

// const bio = function () {
//   return `Streamer Name : ${this.name} and he is usually playing ${this.type}`;
// };

// const elden = bio.bind(gamer1);
// const apex = bio.bind(gamer2);

// console.log(elden());
// console.log(apex());

const gamer1 = {
  name: "Cohh",
};

const gamer2 = {
  name: "Viss",
};

const bio = function (type) {
  return `Streamer Name : ${this.name} and he is usually playing ${type}`;
};

const elden = bio.bind(gamer1, "RPG");
const apex = bio.bind(gamer2, "Apex");

console.log(elden());
console.log(apex());
